#!/usr/bin/env bash

if [[ $(echo $EUID) -ne 0 ]]; then
	printf "run this as root\n"
	exit 1
fi

######################################################################################
## the following commands are taken from https://github.com/tschaffter/raspberry-pi ##
## and changed according to my needs                                                ##
######################################################################################

printf "installing selinux shit...\n"
apt install -y selinux-basics selinux-policy-default auditd
sed -i '$ s/$/ selinux=1 security=selinux/' /boot/cmdline.txt
touch /.autorelabel

printf "YOU SHOULD REBOOT YOUR SYSTEM... NOW! (will take longer due to relabeling)\n"
