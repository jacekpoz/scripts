#!/usr/bin/env bash

if [[ $(echo $EUID) -ne 0 ]]; then
	printf "run this as root\n"
	exit 1
fi

git clone https://github.com/ecdye/zram-config
bash ./zram-config/install.bash

systemctl enable zram-config.service
systemctl start zram-config.service

