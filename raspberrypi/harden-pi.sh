#!/usr/bin/env bash

if [[ $(echo $EUID) -ne 0 ]]; then
	printf "run this as root\n"
	exit 1
fi

######################################################################################################
## the following commands are taken from https://chrisapproved.com/blog/raspberry-pi-hardening.html ##
## and changed according to my needs                                                                ##
######################################################################################################

printf "disabling wireless shit...\n"

if ! grep -q "dtoverlay=pi3-disable-wifi" /boot/config.txt; then
	echo "dtoverlay=pi3-disable-wifi" >> /boot/config.txt
fi
if ! grep -q "dtoverlay=pi3-disable-bt" /boot/config.txt; then
	echo "dtoverlay=pi3-disable-bt" >> /boot/config.txt
fi

printf "configuring unattended upgrades...\n"

apt install -y unattended-upgrades apt-listchanges apticron

# change this to whatever location you want
mv /etc/apt/apt.conf.d/50unattended-upgrades /home/chmura/50unattended-upgrades.bak
wget https://gitlab.com/cgoff/raspberry-pi-hardening/-/raw/master/unattended-upgrades-config/50unattended-upgrades
mv ./50unattended-upgrades /etc/apt/apt.conf.d/

unattended-upgrade -d --dry-run

printf "hardening ssh...\n"

groupadd ssh-users
# change to desired user
usermod -a -G ssh-users chmura

sed -i 's/.*PermitRootLogin.*/PermitRootLogin no/' /etc/ssh/sshd_config
sed -i 's/.*PubkeyAuthentication.*/PubkeyAuthentication yes/' /etc/ssh/sshd_config
sed -i 's/.*PasswordAuthentication.*/PasswordAuthentication no/' /etc/ssh/sshd_config
sed -i 's/.*UsePAM.*/UsePAM no/' /etc/ssh/sshd_config
sed -i 's/.*X11Forwarding.*/X11Forwarding no/' /etc/ssh/sshd_config
sed -i 's/.*AllowGroups.*/AllowGroups ssh-users/' /etc/ssh/sshd_config

systemctl restart ssh

printf "configuring ufw...\n"
apt install -y ufw
ufw default deny incoming
ufw default allow outgoing
ufw limit ssh
ufw allow http
ufw allow https
ufw allow domain
ufw logging on
ufw enable

printf "please reboot :)\n"

