#!/usr/bin/env bash

if [[ $(echo $EUID) -ne 0 ]]; then
	printf "run this as root\n"
	exit 1
fi

######################################################################################
## the following commands are taken from https://github.com/tschaffter/raspberry-pi ##
## and changed according to my needs                                                ##
######################################################################################

# assuming all the debs are in the current dir
# and also that the names are linux-*.deb
VER=$(find . -name 'linux-*.deb' | cut -d '_' -f 2 | cut -d'-' -f 1,2 | head -n 1)

printf "cleaning up after previous installations if there were any...\n"
sudo dpkg --purge "linux-image-$VER"
sudo dpkg --purge "linux-headers-$VER"

printf "installing selinux...\n"
sudo dpkg -i linux-*.deb

KERNEL=$(find /boot -name *vmlinuz* | cut -d'/' -f 3)

if grep -q 'kernel=' /boot/config.txt; then
	sed -i "/.*kernel=.*/kernel=$KERNEL/" /boot/config.txt
else
	echo "kernel=$KERNEL" >> /boot/config.txt
fi

printf "please reboot now and run uname -a to check if the kernel was installed correctly\n"
